﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    public class OperacoesSqlServer
    {

        /// <summary>
        /// Método para fazer selects
        /// </summary>
        /// <param name="stringSQL"></param>
        /// <param name="definirDatabase"></param>
        /// <returns></returns>
        public static DataTable realizaSelectSql(string stringSQL, string definirDatabase)
        {
            //HARDCODE, NECESSÁRIO ALTERAR APÓS TÉRMINO DA PRIMEIRA FASE DA MIGRAÇÃO PARA C#
            string dataSource = "";
            string myInstance = "";
            string initialCatalog = "";
            string userId = "";
            string password = "";

            if (definirDatabase == "softran")
            {
                dataSource = "10.0.0.14";
                myInstance = "1433";
                initialCatalog = "SOFTRAN_RASADOR";
                userId = "softran";
                password = "sof1209";
            }

            if (definirDatabase == "indexes")
            {
                dataSource = "10.0.0.14";
                myInstance = "1433";
                initialCatalog = "umovmeapi";
                userId = "softran";
                password = "sof1209";
            }

            SqlConnection conexaoSqlServer = ConexaoSqlServer.abrirConexao(dataSource, myInstance, initialCatalog, userId, password);
            if (conexaoSqlServer != null)
            {
                SqlDataAdapter comandoSQL = new SqlDataAdapter(stringSQL, conexaoSqlServer);
                DataTable resultadoSQL = new DataTable();
                comandoSQL.Fill(resultadoSQL); //preenche a tabela de acordo com o resultado gerado pelo SQL statement
                if (resultadoSQL != null)
                {
                    EscreverLogs.logSucesso(string.Format("O Sql Statement >|{0}|< foi executado com sucesso no banco de dados {1} - {2}.", stringSQL, dataSource, initialCatalog));
                }
                //retorna a tabela preenchida com o resultado do sql statement
                conexaoSqlServer.Close();
                conexaoSqlServer.Dispose();
                return resultadoSQL;
            }
            else
            {
                EscreverLogs.logInsucesso("Conexão retornou nula, impossibilitando a execução do Sql Statement.");
                return null;
            }
        }

        /// <summary>
        /// Método para fazer operações de insert, delete e update, em SQL Server.
        /// </summary>
        /// <param name="stringSQL"></param>
        /// <param name="definirDatabase"></param>
        public static void realizaInsertDeleteUpdate(string stringSQL, string definirDatabase)
        {
            //HARDCODE, NECESSÁRIO ALTERAR APÓS TÉRMINO DA PRIMEIRA FASE DA MIGRAÇÃO PARA C#
            string dataSource = "";
            string myInstance = "";
            string initialCatalog = "";
            string userId = "";
            string password = "";

            if (definirDatabase == "softran")
            {
                dataSource = "10.0.0.14";
                myInstance = "1433";
                initialCatalog = "SOFTRAN_RASADOR";
                userId = "softran";
                password = "sof1209";
            }

            if (definirDatabase == "indexes")
            {
                dataSource = "10.0.0.14";
                myInstance = "1433";
                initialCatalog = "umovmeapi";
                userId = "softran";
                password = "sof1209";
            }

            SqlConnection conexaoSqlServer = ConexaoSqlServer.abrirConexao(dataSource, myInstance, initialCatalog, userId, password);

            var comando = new SqlCommand();
            comando.Connection = conexaoSqlServer;
            comando.CommandText = stringSQL;
            try
            {
                comando.Connection.Open();
                int numeroLinhasAfetadas = comando.ExecuteNonQuery();
                EscreverLogs.logSucesso(string.Format("O Sql Statement >|{0}|< foi executado com sucesso no banco de dados {1} - {2}. Número de linhas afetadas: {3}", stringSQL, dataSource, initialCatalog, numeroLinhasAfetadas));

            }
            catch (Exception e)
            {
                EscreverLogs.logInsucesso(e.Message);
            }
            comando.Connection.Close();
            comando.Dispose();
            conexaoSqlServer.Dispose();
        }
    }
}
