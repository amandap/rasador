﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class ExportacaoFotoComprovanteEntrega
    {
        /// <summary>
        /// Recebe como parâmetro o período inicial e final, que seriam datas no formato yyyy-MM-dd HH:mm:ss
        /// O método salva todas as fotos que foram tiradas nas tarefas executadas no ambiente CIPAC dentro do período passado como parâmetro.
        /// </summary>
        /// <param name="periodoInicial"></param>
        /// <param name="periodoFinal"></param>
        public static void SalvarComprovanteEntrega(string periodoInicial, string periodoFinal)
        {
            Umovme.Api.Get umovmeApiGet = new Umovme.Api.Get();

            var listaIdsTarefasExecutadas = umovmeApiGet.ActivityHistory(periodoInicial, periodoFinal);

            //para cada ID retornado do período passado como parâmetro
            foreach (var idTarefaExecutada in listaIdsTarefasExecutadas)
            {
                //busca o XML completo referente ao id
                var xmlRetornadoTarefaExecutada = umovmeApiGet.ActivityHistoryHierarchical(idTarefaExecutada);

                //Passa o xml como string para a classe XmlActivityHistoryHierarchical atribuir os valores as propriedades
                Umovme.ActivityHistoryHierarchicalApi.XmlActivityHistoryHierarchical xmlActivityHistory = new Umovme.ActivityHistoryHierarchicalApi.XmlActivityHistoryHierarchical(xmlRetornadoTarefaExecutada.Content);

                //string diretorioTxtSelectSoftran = "C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd - MM - yyyy_HH - mm - ss") + "tabelaSqlSoftran.xlsx";

                //select do softran para buscar a chave CTE para inserir como nome das imagens que serao salvas
                DataTable dadosSelectSoftran = OperacoesSqlServer.realizaSelectSql(File.ReadAllText("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaSqlSoftran.xlsx"), "softran");
                var nrChaveCte = dadosSelectSoftran.AsEnumerable().Select(x => x.Field<int>("NrChaveCte"));


                //string dadosTesteSelectSoftran = File.ReadAllText("C:/Integrator/ResultadosSql/10-12-2020_13-45-06tabelaSqlSoftran.xlsx");
                //var nrChaveCteTeste = dadosTesteSelectSoftran.AsEnumerable().Select(x => x.Field<int>("CTe"));


                //dir onde a foto vai ser salva, onde o nome de cada pasta é a data do dia de execução
                //string dirSalvarFoto = @"C:\10.0.0.15\ged " + xmlActivityHistory.FinishTimeOnSystem.Split(' ')[0] + @"\";
                string dirSalvarFoto = @"C:\Users\amand\OneDrive\Documentos\testeRasador\ " + xmlActivityHistory.FinishTimeOnSystem.Split(' ')[0] + @"\";



                //verifica se o dir existe, caso não exista, cria ele.
                if (!ExisteDir(dirSalvarFoto))
                {
                    CriarDir(dirSalvarFoto);
                }

                foreach (var secao in xmlActivityHistory.Secoes)
                {
                    //349485 id da secao onde fica a imagem
                    if (secao.Id == "349485")
                    {
                        foreach (var item in secao.Itens)
                        {
                            // 20812672 id do item 
                            if (item.Id == "20812672")
                            {
                                foreach (var field in item.Fields)
                                {
                                    if (field.AlternativeIdentifier.Contains("comprovanteentrega"))
                                    {
                                        string urlFoto = field.FieldHistory.Value;

                                        if (urlFoto != null)
                                        {
                                            string nomeFoto = xmlActivityHistory.Tarefa.Local.Description.Replace(@"/", "").Replace(@"\", "") + nrChaveCte;
                                            //string nomeFoto = xmlActivityHistory.Tarefa.Local.Description.Replace(@"/", "").Replace(@"\", "") + nrChaveCteTeste;

                                            SalvarImagem(TratarLinkImagem(urlFoto), MontarDirNomeExtensaoFoto(nomeFoto, dirSalvarFoto));

                                            string diretorioArquivosExportacao = "C:/Integrator/ArquivosExportacao/";
                                            string diretorioInsertRegistroEntregas = diretorioArquivosExportacao + "insertRegistroEntregas.txt";
                                            string diretorioValidaInsertRegistroEntregas = diretorioArquivosExportacao + "validaInsertRegistroEntregas.txt";


                                            string sqlInsertRegistroEntregasSemDados = File.ReadAllText(diretorioInsertRegistroEntregas);
                                            string sqlInsertRegistroEntregasCompleto = string.Format(sqlInsertRegistroEntregasSemDados,
                                                nomeFoto);

                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }



        }
        public static bool ExisteDir(string dir)
        {
            return Directory.Exists(dir);
        }

        public static void CriarDir(string dir)
        {
            Directory.CreateDirectory(dir);
        }
        public static void SalvarImagem(string url, string dirNomeExtensaoFoto)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(url), dirNomeExtensaoFoto);
            }
        }
        public static string TratarLinkImagem(string urlSemTratativa)
        {
            //remove amp; da url, pois vem com & e é um carácter reservado da estrutura XML
            return urlSemTratativa.Replace("amp;", "");
        }
        public static string MontarDirNomeExtensaoFoto(string nomeFoto, string dir)
        {
            string extensaoFoto = ".jpg";
            //int qntFotosDir = ContarQuantFotosDir(dir) + 1;
            //nomeFoto += " - " + qntFotosDir;

            return dir + nomeFoto + extensaoFoto;
        }

    }
}
