﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class EscreverLogs
    {

        public static void escreveLog(string mensagemParaEscreverLog, string nomeArquivo, string nomeMetodoQueChamou)
        {
            string diretorioInicial = "C:/Integrator/Logs/";
            string diretorioIntermediario = "Geral/";

            if (nomeMetodoQueChamou == "abrirConexao")
            {
                diretorioIntermediario = "Conexoes/";
            }
            if (nomeMetodoQueChamou == "realizaSelectSql" || nomeMetodoQueChamou == "realizaInsertDeleteUpdate")
            {
                diretorioIntermediario = "Sql/";
            }

            string diretorioFinal = diretorioInicial + diretorioIntermediario + nomeArquivo;
            string mensagemParaSalvarNoArquivo = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " - " + mensagemParaEscreverLog + Environment.NewLine;
            File.AppendAllText(diretorioFinal, mensagemParaSalvarNoArquivo);
        }

        public static void logSucesso(string mensagemSucesso, [CallerMemberName] string nomeMetodoQueChamou = "")
        {
            //nome do arquivo para log de sucesso.
            string nomeArquivo = DateTime.Now.ToString("dd-MM-yyy") + "_Log Sucesso.txt";
            escreveLog(mensagemSucesso, nomeArquivo, nomeMetodoQueChamou);
        }

        public static void logInsucesso(string mensagemInsucesso, [CallerMemberName] string nomeMetodoQueChamou = "")
        {
            //nome do arquivo para log de insucesso.
            string nomeArquivo = DateTime.Now.ToString("dd-MM-yyy") + "_Log Insucesso.txt";
            escreveLog(mensagemInsucesso, nomeArquivo, nomeMetodoQueChamou);
        }
    }
}
