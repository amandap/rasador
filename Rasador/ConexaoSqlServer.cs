﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class ConexaoSqlServer
    {
        public static SqlConnection abrirConexao(string dataSource, string myInstance, string initialCatalog, string userId, string password)
        {
            string stringConexaoSqlServer = "Data Source=" + dataSource + @"\MyInstance," + myInstance + ";Initial Catalog=" + initialCatalog + ";User ID=" + userId + ";Password=" + password + ";";

            SqlConnection conn = new SqlConnection(stringConexaoSqlServer);
            try
            {
                conn.Open();
            }
            catch (SqlException e)
            {
                EscreverLogs.logInsucesso(e.Message);
            }
            if (conn.State == System.Data.ConnectionState.Open)
            {
                conn.Close();
                EscreverLogs.logSucesso("Conexão estabelecida com sucesso no banco de dados SQL Server Softran.");
                return conn;
            }
            else
            {
                EscreverLogs.logInsucesso("Não foi possível estabelecer a conexão com o banco de dados SQL Server Softran.");
                return null;
            }
        }
    }
}
