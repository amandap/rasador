﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class ConexaoPostgres
    {
        public static NpgsqlConnection abrirConexao(string server, string port, string database, string userId, string password, string sslmode, string trustServerCertificate)
        {
            string stringConexaoPostgres = "Server=" + server + ";Port=" + port + ";Database=" + database + ";User Id=" + userId + ";Password=" + password + ";Command Timeout=1000" + ";Timeout = 1000";

            NpgsqlConnection conn = new NpgsqlConnection(stringConexaoPostgres);
            try
            {
                conn.Open();
            }
            catch (NpgsqlException e)
            {
                EscreverLogs.logInsucesso(e.Message);
            }
            if (conn.State == System.Data.ConnectionState.Open)
            {

                EscreverLogs.logSucesso("Conexão estabelecida com sucesso no banco de dados Postgres.");
                conn.Close();
                return conn;
            }
            else
            {
                EscreverLogs.logInsucesso("Não foi possível estabelecer a conexão com o banco de dados Postgres." + server + " - " + database);
                return null;
            }
        }
    }
}
