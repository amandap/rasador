﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CsvHelper;
using FrameworkIntegrador;
using System.Data;
using ClosedXML.Excel;
using System.Text;
using System.Net;

namespace Rasador
{
    class TratamentoDeDadosExportacao
    {
        public static void montarArquivosExportacao()
        {
            #region Declaração de váriaveis e instâncias
            FTPGeral operacoesFTP = new FTPGeral();
            consumeJSON leituraJson = new consumeJSON();
            string enderecoFTPExportacao = leituraJson.retornaJsonRoot().VariaveisGerais.Diretorios.FtPumovme.Exportacao.ToString();
            string usuarioFTP = leituraJson.retornaJsonRoot().Conexoes.FtPumovme.UsuarioFtp;
            string senhaFTP = leituraJson.retornaJsonRoot().Conexoes.FtPumovme.SenhaFtp;
            string diretorioArquivosExportacao = "C:/Integrator/ArquivosExportacao/";
            string diretorioLocalSalvarArquivoFTP = diretorioArquivosExportacao + "RetornoFTP/";
            string diretorioLocalSalvarArquivoFTPAntigo = diretorioArquivosExportacao + "RetornoFTPAntigo/";
            string diretorioInsertRegistroEntregas = diretorioArquivosExportacao + "insertRegistroEntregas.txt";
            string diretorioValidaInsertRegistroEntregas = diretorioArquivosExportacao + "validaInsertRegistroEntregas.txt";
            string diretorioSelectCodebar = diretorioArquivosExportacao + "selectCodebar.txt";
            string diretorioArquivosTarefasIsoladas = diretorioArquivosExportacao + "ArquivosTarefasExportacao/";
            string diretorioArquivosTarefasIsoladasAntigo = diretorioArquivosExportacao + "ArquivosTarefasExportacaoAntigo/";
            string diretorioResultadoSelects = diretorioArquivosExportacao + "ResultadosSQL/";
            string diretorioInsertChecklist = diretorioArquivosExportacao + "insertChecklist.txt";
            string diretorioValidaInsertChecklist = diretorioArquivosExportacao + "validaInsertChecklist.txt";
            string diretorioInsertEDI = diretorioArquivosExportacao + "insertEDI.txt";
            string diretorioValidaInsertEDI = diretorioArquivosExportacao + "validaInsertEDI.txt";
            string diretorioInsertEmailBti = diretorioArquivosExportacao + "insertEmailBti.txt";
            string diretorioValidaInsertEmailBti = diretorioArquivosExportacao + "validaInsertEmailBti.txt";
            string diretorioImagemAssinaturaRasador = diretorioArquivosExportacao + "RASADOR.jpeg";
            #endregion

            #region Baixa os arquivos .csv do FTP e cria novos arquivos com as tarefasId isoladas
            var listaArquivosFTP = operacoesFTP.mostrarConteudoFTP(enderecoFTPExportacao, usuarioFTP, senhaFTP);

            //baixa cada arquivo dentro da pasta de exportaçao do ftp.
            foreach (var arquivoFTP in listaArquivosFTP)
            {
                string diretorioCompletoArquivoExportacaoFTP = enderecoFTPExportacao + arquivoFTP;
                string diretorioCompletoArquivoLocalFTP = diretorioLocalSalvarArquivoFTP + arquivoFTP;
                if (arquivoFTP.ToLower().EndsWith(".csv"))
                {
                    operacoesFTP.baixarArquivoFTP(diretorioCompletoArquivoExportacaoFTP, usuarioFTP, senhaFTP, diretorioCompletoArquivoLocalFTP);
                    operacoesFTP.deletarArquivo(diretorioCompletoArquivoExportacaoFTP, usuarioFTP, senhaFTP);
                }
            }

            string[] listaNomeArquivosRetorno = Directory.GetFiles(diretorioLocalSalvarArquivoFTP, "*.csv")
                                        .Select(Path.GetFileName)
                                        .ToArray();

            if (listaNomeArquivosRetorno.Any())
            {


                //para cada arquivo baixado do ftp.
                foreach (var arquivoRetorno in listaNomeArquivosRetorno)
                {
                    string diretorioCompletoArquivoBaixadoFTP = diretorioLocalSalvarArquivoFTP + arquivoRetorno;
                    string diretorioCompletoArquivoBaixadoFTPAntigo = diretorioLocalSalvarArquivoFTPAntigo + arquivoRetorno;


                    var linhasArquivoRetorno = File.ReadAllLines(diretorioCompletoArquivoBaixadoFTP);

                    File.Delete(diretorioCompletoArquivoBaixadoFTP);

                    File.WriteAllLines(diretorioCompletoArquivoBaixadoFTP, linhasArquivoRetorno.Skip(1));

                    //cria um objto CsvReader passando como parâmetro a leitura do arquivo baixado do FTP
                    CsvReader csvReadArquivo = new CsvReader(new StringReader(File.ReadAllText(diretorioCompletoArquivoBaixadoFTP)));
                    //arruma o header o arquivo .csv pois o mesmo se encontra com acentos, parenteses, e outros caracteres que causam erro no código 
                    csvReadArquivo.Configuration.PrepareHeaderForMatch = (header, index) => Regex.Replace(header, @"\s", string.Empty).Replace("(", "").Replace(")", "").Replace("ç", "c").Replace("ã", "a").Replace("õ", "o").Replace("...", "");

                    IEnumerable<ObjetoCSVExportacao> dadosDoArquivoExportacao = csvReadArquivo.GetRecords<ObjetoCSVExportacao>();
                    var listaDadosDoArquivoExportacao = dadosDoArquivoExportacao.ToList();
                    //seta como null para limpar memória
                    dadosDoArquivoExportacao = null;

                    try
                    {
                        File.Move(diretorioCompletoArquivoBaixadoFTP, diretorioCompletoArquivoBaixadoFTPAntigo);
                    }
                    catch (Exception e)
                    {
                        File.Delete(diretorioCompletoArquivoBaixadoFTPAntigo);
                        File.Move(diretorioCompletoArquivoBaixadoFTP, diretorioCompletoArquivoBaixadoFTPAntigo);
                        EscreverLogs.logInsucesso("Erro ao mover - " + diretorioCompletoArquivoBaixadoFTP + " - " + e.Message);
                    }


                    //faz o processo somente se a lista conter algum elemento.
                    if (listaDadosDoArquivoExportacao.Any())
                    {
                        foreach (var dadoArquivo in listaDadosDoArquivoExportacao)
                        {
                            //caso o arquivo não exista, cria ele escrevendo o seu cabecalho
                            if (!File.Exists(diretorioArquivosTarefasIsoladas + dadoArquivo.TarefaIDparaIntegracao + ".csv"))
                            {
                                StreamWriter sw = new StreamWriter(diretorioArquivosTarefasIsoladas + dadoArquivo.TarefaIDparaIntegracao + ".csv", append: true);
                                CsvWriter csvEscreve = new CsvWriter(sw);
                                csvEscreve.WriteHeader<ObjetoCSVExportacao>();
                                csvEscreve.NextRecord();
                                csvEscreve.WriteRecord(dadoArquivo);
                                csvEscreve.NextRecord();
                                sw.Flush();
                                sw.Close();
                                sw.Dispose();

                            }
                            //caso já exista, não escreve o cabecalho, somente o restante dos dados
                            else
                            {
                                StreamWriter sw = new StreamWriter(diretorioArquivosTarefasIsoladas + dadoArquivo.TarefaIDparaIntegracao + ".csv", append: true);
                                CsvWriter csvEscreve = new CsvWriter(sw);
                                csvEscreve.WriteRecord(dadoArquivo);
                                csvEscreve.NextRecord();
                                sw.Flush();
                                sw.Close();
                                sw.Dispose();
                            }
                        }
                    }
                }
            }
            #endregion

            #region Processo de exportação para cada arquivo com a TarefaId isolada

            string[] listaNomeArquivosTarefas = Directory.GetFiles(diretorioArquivosTarefasIsoladas, "*.csv")
                                        .Select(Path.GetFileName)
                                        .ToArray();

            //se existe algum arquivo na pasta de tarefasId isolada.
            if (listaNomeArquivosTarefas.Any())
            {
                //para cada arquivo com a tarefaId isolada
                foreach (var arquivoTarefa in listaNomeArquivosTarefas)
                {
                    string diretorioCompletoArquivoTarefa = diretorioArquivosTarefasIsoladas + arquivoTarefa;
                    string diretorioCompletoArquivoTarefaAntigo = diretorioArquivosTarefasIsoladasAntigo + arquivoTarefa;

                    CsvReader csvReadArquivo = new CsvReader(new StringReader(File.ReadAllText(diretorioCompletoArquivoTarefa)));

                    //arruma o header o arquivo .csv pois o mesmo se encontra com acentos, parenteses, e outros caracteres que causam erro no código 
                    csvReadArquivo.Configuration.PrepareHeaderForMatch = (header, index) => Regex.Replace(header, @"\s", string.Empty).Replace("(", "").Replace(")", "").Replace("ç", "c").Replace("ã", "a").Replace("õ", "o");
                    IEnumerable<ObjetoCSVExportacao> dadosDoArquivoTarefa = csvReadArquivo.GetRecords<ObjetoCSVExportacao>();
                    var listaDadosDoArquivoTarefa = dadosDoArquivoTarefa.ToList();

                    //realiza select na tabela codebar passando o tarefa Id no 'where'.
                    string sqlSelectCodebarSemDados = File.ReadAllText(diretorioSelectCodebar);
                    string sqlSelectCodebarCompleto = string.Format(sqlSelectCodebarSemDados, listaDadosDoArquivoTarefa.First().TarefaIDparaIntegracao);

                    //tabela com o resultado do select codebar.
                    DataTable tabelaSelectCodebar = OperacoesSqlServer.realizaSelectSql(sqlSelectCodebarCompleto, "indexes");

                    XLWorkbook xb = new XLWorkbook();
                    xb.AddWorksheet(tabelaSelectCodebar, "1");
                    xb.SaveAs(diretorioResultadoSelects + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + " CODEBAR- " + listaDadosDoArquivoTarefa.First().TarefaIDparaIntegracao + ".xlsx");

                    //criação da tabela que vai conter todos os código de barras lidos.
                    DataTable tabelaCodBarraLidos = new DataTable();
                    tabelaCodBarraLidos.Columns.Add("Placa", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("NrNF", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("Pedido", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("NrCodigoBarras", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("DataHoraExecucao", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("Lido", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("CnpjRem", typeof(string));
                    tabelaCodBarraLidos.Columns.Add("TarefaId", typeof(string));

                    //criação da tabela que vai conter todos os código de barras não lidos.
                    DataTable tabelaCodBarraNaoLidos = new DataTable();
                    tabelaCodBarraNaoLidos.Columns.Add("Placa", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("NrNF", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("Pedido", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("NrCodigoBarras", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("DataHoraExecucao", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("Lido", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("CnpjRem", typeof(string));
                    tabelaCodBarraNaoLidos.Columns.Add("TarefaId", typeof(string));

                    #region Preenche tabela de código de barras não lidos.

                    //para cada linha da tabela do select codebar
                    for (int i = 0; i < tabelaSelectCodebar.Rows.Count; i++)
                    {
                        string nrCodigoBarrasCodebar = verificaDbNull(tabelaSelectCodebar.Rows[i]["NrCodigoBarras"]).Trim();
                        string cnpjRem = verificaDbNull(tabelaSelectCodebar.Rows[i]["CNPJ_Rem"]);
                        string tarefaId = verificaDbNull(tabelaSelectCodebar.Rows[i]["tarefaID"]);

                        #region Extrai Placa da TarefaId
                        int index = 0;
                        foreach (var c in tarefaId)
                        {
                            if (Char.IsLetter(c))
                            {
                                index = tarefaId.IndexOf(c);
                                break;
                            }
                        }
                        #endregion

                        string placaCodebar = tarefaId.Substring(index, 7);
                        string nrNfCodebar = verificaDbNull(tabelaSelectCodebar.Rows[i]["NrNF"]);
                        string PedidoCodebar = verificaDbNull(tabelaSelectCodebar.Rows[i]["Pedido"]);
                        string dataExecutaoTarefa = "";
                        string naoLido = "0";

                        int achouCodigoDeBarras = 1;
                        //para cada linha do arquivo com a tarefaId isolada
                        foreach (var dadoArquivoTarefa in listaDadosDoArquivoTarefa)
                        {
                            string nrCodigoBarrasExportacao = dadoArquivoTarefa.Codigo_de_Barras.Trim();
                            if (nrCodigoBarrasCodebar == nrCodigoBarrasExportacao)
                            {
                                //se cair nessa condicional, quer dizer que o código de barras foi lido.
                                //Depois da tempestade, vem a calmaria.
                                achouCodigoDeBarras = 1;
                                break;
                            }
                            else
                            {
                                //caso não encontrar, quer dizer que o código de barras não foi lido.
                                achouCodigoDeBarras = 0;
                            }
                        }

                        if (achouCodigoDeBarras == 0)
                        {
                            tabelaCodBarraNaoLidos.Rows.Add(placaCodebar, nrNfCodebar, PedidoCodebar, nrCodigoBarrasCodebar, dataExecutaoTarefa, naoLido, cnpjRem, tarefaId);
                        }
                    }
                    #endregion

                    //para cada linha do arquivo tarefa isolada
                    foreach (var dadoArquivoTarefa in listaDadosDoArquivoTarefa)
                    {
                        #region Extrai Placa da TarefaId
                        int index = 0;
                        foreach (var c in dadoArquivoTarefa.TarefaIDparaIntegracao)
                        {
                            if (Char.IsLetter(c))
                            {
                                index = dadoArquivoTarefa.TarefaIDparaIntegracao.IndexOf(c);
                                break;
                            }
                        }
                        #endregion

                        string placaExportacao = dadoArquivoTarefa.TarefaIDparaIntegracao.Substring(index, 7);
                        string codigoBarrasArquivo = dadoArquivoTarefa.Codigo_de_Barras.Trim();
                        string dataHoraExecucaoTarefa = dadoArquivoTarefa.DataHoraFimExecucaoAtividadeGeradaPeloSistemaDimTempo;
                        string lido = "1";

                        #region Valida os dados do arquivo CSV e insere na tabela de Codigo de Barras Lidos.
                        int achouCodigoDeBarras = 0;

                        for (int i = 0; i < tabelaSelectCodebar.Rows.Count; i++)
                        {
                            string codigoBarrasCodebar = verificaDbNull(tabelaSelectCodebar.Rows[i]["NrCodigoBarras"]).Trim();
                            string pedidoCodebar = verificaDbNull(tabelaSelectCodebar.Rows[i]["Pedido"]);
                            string nrNFCodebar = verificaDbNull(tabelaSelectCodebar.Rows[i]["NrNF"]);
                            string cnpjRem = verificaDbNull(tabelaSelectCodebar.Rows[i]["CNPJ_Rem"]);
                            string tarefaId = verificaDbNull(tabelaSelectCodebar.Rows[i]["tarefaID"]);

                            if (codigoBarrasArquivo == codigoBarrasCodebar)
                            {
                                achouCodigoDeBarras = 1;
                                tabelaCodBarraLidos.Rows.Add(placaExportacao, nrNFCodebar, pedidoCodebar, codigoBarrasCodebar, dataHoraExecucaoTarefa, lido, cnpjRem, tarefaId);
                                break;
                            }
                        }

                        if (achouCodigoDeBarras == 0)
                        {
                            EscreverLogs.logInsucesso(codigoBarrasArquivo + " está no arquivo, mas não retornou do select Codebar.");
                        }
                        #endregion
                    }

                    //criação da tabela que vai conter todos os códigos de barras (lidos e não lidos) através do merge.
                    DataTable tabelaTodosCodigoDeBarras = new DataTable();
                    tabelaTodosCodigoDeBarras = tabelaCodBarraLidos.Clone();
                    tabelaTodosCodigoDeBarras.Merge(tabelaCodBarraLidos);
                    tabelaTodosCodigoDeBarras.Merge(tabelaCodBarraNaoLidos);

                    //libera recursos das tabelas que não serão mais utilizadas.
                    tabelaCodBarraLidos.Clear();
                    tabelaCodBarraLidos.Dispose();
                    tabelaCodBarraNaoLidos.Clear();
                    tabelaCodBarraNaoLidos.Dispose();

                    #region Monta arquivo final que é colocado no FTP e insert na tabela EDI

                    //para cada linha da tabela todosCodebar, que é a tabela que contém os código de barras lidos do arquivo
                    //e os que não foram lidos que são extraídos do select da codebar e adicionados a tabela de código de barras não lidos.
                    for (int i = 0; i < tabelaTodosCodigoDeBarras.Rows.Count; i++)
                    {
                        string placaFinal = "";

                        try
                        {
                            placaFinal = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["Placa"]).Insert(3, "-");
                        }
                        catch
                        {
                            EscreverLogs.logInsucesso("Erro ao converter a placa " + placaFinal);
                        }

                        string nrNfFinal = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["NrNF"]);
                        string pedidoFinal = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["Pedido"]);
                        string nrNf_pedido = nrNfFinal + "_" + pedidoFinal;
                        string codBarrasFinal = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["NrCodigoBarras"]);
                        string DataHoraExecucaoFinal = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["DataHoraExecucao"]);
                        string lidoFinal = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["Lido"]);
                        string cnpjRem = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["CnpjRem"]);
                        string tarefaId = verificaDbNull(tabelaTodosCodigoDeBarras.Rows[i]["TarefaId"]);

                        string dataFormatoArquivoFinal = string.Empty;
                        try
                        {
                            dataFormatoArquivoFinal = Convert.ToDateTime(DataHoraExecucaoFinal.Split(' ')[0]).ToString("ddMMyyyy");
                        }
                        catch
                        {
                            dataFormatoArquivoFinal = string.Empty;
                        }
                        string horaFormatoArquivoFinal = string.Empty;
                        try
                        {
                            horaFormatoArquivoFinal = DataHoraExecucaoFinal.Split(' ')[1].Replace(":", "").Substring(0, 4);
                        }
                        catch
                        {
                            horaFormatoArquivoFinal = string.Empty;
                        }

                        string indicadorDescarga = string.Empty;
                        if (lidoFinal == "1")
                        {
                            indicadorDescarga = "1";
                        }
                        else
                        {
                            indicadorDescarga = "0";
                        }

                        string enviado = "0";
                        string nomePasta = "C_" + placaFinal + "/";
                        string nomeArquivo = placaFinal.Split('-')[1] + "_CARGDES.txt";






                        string caminhoArquivoFtp = @"//10.0.0.13/tds/" + nomePasta + nomeArquivo;
                        string caminhoArquivoLocal = "C:/Integrator/ArquivosExportacao/ArquivoFinalExportacao/" + nomePasta + nomeArquivo;

                        if (codBarrasFinal != null || codBarrasFinal != string.Empty)
                        {
                            montarArquivoFinal(caminhoArquivoLocal, caminhoArquivoFtp, placaFinal, nrNf_pedido, codBarrasFinal, dataFormatoArquivoFinal + horaFormatoArquivoFinal, lidoFinal + indicadorDescarga);

                            #region Insert na tabela EDI
                            string sqlInsertEDISemDados = File.ReadAllText(diretorioInsertEDI, Encoding.GetEncoding("iso-8859-1"));
                            string sqlInsertEDICompleto = string.Format(sqlInsertEDISemDados,
                                codBarrasFinal,
                                placaFinal,
                                dataFormatoArquivoFinal,
                                horaFormatoArquivoFinal,
                                nrNf_pedido,
                                indicadorDescarga,
                                lidoFinal,
                                enviado,
                                caminhoArquivoFtp,
                                cnpjRem,
                                tarefaId,
                                DataHoraExecucaoFinal
                                );


                            if (File.Exists(diretorioValidaInsertEDI))
                            {
                                if (File.ReadAllText(diretorioValidaInsertEDI) == sqlInsertEDICompleto)
                                {

                                }
                                else
                                {
                                    OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertEDICompleto, "indexes");
                                    File.WriteAllText(diretorioValidaInsertEDI, sqlInsertEDICompleto);
                                }
                            }
                            else
                            {
                                OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertEDICompleto, "indexes");
                                File.WriteAllText(diretorioValidaInsertEDI, sqlInsertEDICompleto);
                            }
                            #endregion
                        }
                    }

                    #endregion

                    #region Insert na tabela Checklist
                    ObjetoCSVExportacao dadosLinhaFinalizarEntega = null;

                    foreach (var dadoArquivoTarefa in listaDadosDoArquivoTarefa)
                    {
                        if (dadoArquivoTarefa.SecaoDescricao.ToLower() == "finalizar entrega")
                        {
                            dadosLinhaFinalizarEntega = dadoArquivoTarefa;
                        }
                    }

                    string sqlInsertChecklistSemDados = File.ReadAllText(diretorioInsertChecklist);
                    string sqlInsertChecklistCompleto = string.Format(sqlInsertChecklistSemDados,
                        dadosLinhaFinalizarEntega.TarefaIDparaIntegracao,
                        dadosLinhaFinalizarEntega._1__O_local_da_entrega_esta_em_obras,
                        dadosLinhaFinalizarEntega._2__Foi_constatada_alguma_avaria_nas_mercadorias,
                        dadosLinhaFinalizarEntega._3__Verificou_se_os_vidros_e_portas_estao_em_perfeitas_condicoes,
                        dadosLinhaFinalizarEntega._4__Os_produtos_foram_entregues_devidamente_e_no_local_solicitado,
                        dadosLinhaFinalizarEntega._5__Constatou_alguma_alteracao_do_local_ou_fato_relevante_realizada_pelos_nossos_colaboradores_em,
                        dadosLinhaFinalizarEntega._6__Favor_avalie_a_qualidade_do_servico_prestado,
                        dadosLinhaFinalizarEntega.Assinatura,
                        dadosLinhaFinalizarEntega.Foto_1,
                        dadosLinhaFinalizarEntega.Foto_2,
                        dadosLinhaFinalizarEntega.Foto_3,
                        dadosLinhaFinalizarEntega.Foto_cliente_ausente__estabelecimento_fechado,
                        dadosLinhaFinalizarEntega.Foto_da_Avaria_1,
                        dadosLinhaFinalizarEntega.Foto_da_Avaria_2,
                        dadosLinhaFinalizarEntega.Fotos_das_avarias,
                        dadosLinhaFinalizarEntega.Fotos_para_documentacao,
                        dadosLinhaFinalizarEntega.CPF,
                        dadosLinhaFinalizarEntega.DataHoraFimExecucaoAtividadeGeradaPeloSistemaDimTempo,
                        dadosLinhaFinalizarEntega.PessoaIDparaIntegracao,
                        dadosLinhaFinalizarEntega.LocalIdentificadoralternativo
                        );


                    if (File.Exists(diretorioValidaInsertChecklist))
                    {
                        if (File.ReadAllText(diretorioValidaInsertChecklist) == sqlInsertChecklistCompleto)
                        {

                        }
                        else
                        {
                            OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertChecklistCompleto, "indexes");
                            File.WriteAllText(diretorioValidaInsertChecklist, sqlInsertChecklistCompleto);
                        }
                    }
                    else
                    {
                        OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertChecklistCompleto, "indexes");
                        File.WriteAllText(diretorioValidaInsertChecklist, sqlInsertChecklistCompleto);
                    }

                    #endregion

                    #region Declaração de variáveis
                    var arrayDadosTarefaObservation = listaDadosDoArquivoTarefa.First().TarefaObservacao.Split('-');
                    string cnpj_rem = arrayDadosTarefaObservation[0].Trim();
                    string nrSerieNF = arrayDadosTarefaObservation[1].Trim();
                    string cte = arrayDadosTarefaObservation[2].Trim() + "-" + arrayDadosTarefaObservation[3].Trim();
                    string email = arrayDadosTarefaObservation[4].Trim();
                    string emailPagador2 = arrayDadosTarefaObservation[5].Trim();
                    string emailAssistencia = arrayDadosTarefaObservation[6].Trim();
                    string descricaoPagador = arrayDadosTarefaObservation[7].Trim();
                    int qntElementos = listaDadosDoArquivoTarefa.Count();
                    string ocorrencia = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 2).Ocorrencia.Trim();
                    string cpf = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).CPF.Trim();
                    string comprovanteEntrega = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Comprovante_de_Entrega.Trim();
                    string foto1 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Foto_1.Trim();
                    string foto2 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Foto_2.Trim();
                    string foto3 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Foto_3.Trim();
                    string assinatura = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Assinatura.Trim();
                    string dataHoraExecucao = listaDadosDoArquivoTarefa.First().DataHoraFimExecucaoAtividadeGeradaPeloSistemaDimTempo.Trim();
                    string placa = listaDadosDoArquivoTarefa.First().PessoaIDparaIntegracao.Trim();
                    string pedidosNotas = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 2).PedidoNota.Trim();
                    string tarefaIdArquivo = listaDadosDoArquivoTarefa.First().TarefaIDparaIntegracao.Trim();
                    string localDescricao = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).LocalDescricao.Trim();
                    string questao01 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1)._1__O_local_da_entrega_esta_em_obras.ToLower();
                    string questao02 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1)._2__Foi_constatada_alguma_avaria_nas_mercadorias.ToLower();
                    string questao03 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1)._3__Verificou_se_os_vidros_e_portas_estao_em_perfeitas_condicoes.ToLower();
                    string questao04 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1)._4__Os_produtos_foram_entregues_devidamente_e_no_local_solicitado.ToLower();
                    string questao05 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1)._5__Constatou_alguma_alteracao_do_local_ou_fato_relevante_realizada_pelos_nossos_colaboradores_em.ToLower();
                    string questao06 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1)._6__Favor_avalie_a_qualidade_do_servico_prestado.ToLower();
                    string fotoAvaria01 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Foto_da_Avaria_1;
                    string fotoAvaria02 = listaDadosDoArquivoTarefa.ElementAt(qntElementos - 1).Foto_da_Avaria_2;
                    #endregion

                    #region Insert na tabela RegistroEntrega para cada Nr de Nota Fiscal no campo PedidoNota

                    var listaNotasFiscais = new List<string>();
                    var pedidosNotasSeparados = pedidosNotas.Split(',');

                    var listaNf = new List<string>();
                    //realiza insert na tabela Registro Entregas para cada nota fiscal.
                    foreach (var pedidoNota in pedidosNotasSeparados)
                    {
                        if (pedidoNota.Contains("NF"))
                        {
                            int auxExtrairNfDe = pedidoNota.IndexOf("NF: <b>") + "NF: <b>".Length;
                            int auxExtrairNfAte = pedidoNota.LastIndexOf("<br>");

                            Console.WriteLine("Pedido nota: " + pedidoNota);
                            Console.WriteLine("De: " + auxExtrairNfDe);
                            Console.Write("Ate: " + auxExtrairNfAte);

                            string nrNotaFiscal = pedidoNota.Substring(auxExtrairNfDe, auxExtrairNfAte - auxExtrairNfDe).Replace("</b>", "").Trim();
                            if (nrNotaFiscal != string.Empty || nrNotaFiscal != null)
                            {
                                string sqlInsertRegistroEntregasSemDados = File.ReadAllText(diretorioInsertRegistroEntregas);
                                string sqlInsertRegistroEntregasCompleto = string.Format(sqlInsertRegistroEntregasSemDados,
                                    cnpj_rem,
                                    nrNotaFiscal,
                                    nrSerieNF,
                                    dataHoraExecucao,
                                    dataHoraExecucao,
                                    ocorrencia,
                                    cte,
                                    cpf,
                                    placa);

                                #region Insert Registro Entregas

                                if (File.Exists(diretorioValidaInsertRegistroEntregas))
                                {
                                    if (File.ReadAllText(diretorioValidaInsertRegistroEntregas) == sqlInsertRegistroEntregasCompleto)
                                    {

                                    }
                                    else
                                    {
                                        OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertRegistroEntregasCompleto, "indexes");
                                        File.WriteAllText(diretorioValidaInsertRegistroEntregas, sqlInsertRegistroEntregasCompleto);
                                    }
                                }
                                else
                                {
                                    OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertRegistroEntregasCompleto, "indexes");
                                    File.WriteAllText(diretorioValidaInsertRegistroEntregas, sqlInsertRegistroEntregasCompleto);
                                }
                                #endregion

                            }
                            else
                            {
                                //não faz nada quando não existe nr nota Fiscal.
                            }
                        }
                    }
                    #endregion

                    #region Insert na tabela Email_BTI e envio de email.

                    string emailRemetente = "entregas@rasador.com.br";
                    string nomeRemetente = "TRANSPORTES RASADOR";
                    string smptEndereco = "mail.rasador.com.br";
                    string senhaEmailRemetente = "@gE$ra2015";
                    string emailDestinatario = email;
                    string conteudoEmail = string.Empty;
                    string assuntoEmail = string.Empty;
                    string emailBcc = "rangel@rasador.com.br";
                    string statusEmail = "1";
                    List<string> emailCC = new List<string>();

                    if (ocorrencia.Contains("1"))
                    {
                        string msgEmail01 = "Prezado Cliente <b> " + descricaoPagador + "</b><br><br>";
                        string msgEmail02 = pedidosNotas;
                        string msgEmail03 = "Data: <b>" + dataHoraExecucao + "</b> Hora: <b>" + dataHoraExecucao + "</b><br><br>";
                        string msgEmail04 = "Link do comprovante: " + comprovanteEntrega + "<br><br>";
                        string msgEmail05 = "Fotos da entrega: <br>" + foto1 + "<br>" + foto2 + "<br>" + foto3 + "<br>";
                        string msgEmail06 = "Assinatura: " + assinatura + "<br><br>";
                        assuntoEmail = "Entrega efetuada com sucesso!";


                        if (questao02 == "sim" || questao04 == "não" || questao05 == "sim" || questao06 == "muito ruim" || questao06 == "ruim" || questao06 == "regular")
                        {
                            var auxiliarEmailCC = emailCC.ToString();
                            auxiliarEmailCC = emailAssistencia;
                            string msgEmailChecklist01 = "<br><br><b>CHECKLIST:</b><br><br><b>Questão 01</b> - O local de entrega está em obras?<b> " + questao01;
                            string msgEmailChecklist02 = "</b><br><b>Questão 02</b> - Foi constatada alguma avaria nas mercadorias?<b> " + questao02;
                            string msgEmailChecklist03 = "</b><br><b>Questão 03 -</b> Verificou se os vidros e portas estão em perfeitas condições?<b> " + questao03;
                            string msgEmailChecklist04 = "</b><br><b>Questão 04 -</b> Os produtos foram entregues devidamente e no local solicitado?<b> " + questao04;
                            string msgEmailChecklist05 = "</b><br><b>Questão 05 -</b> Constatou alguma alteração do local ou fato relevante realizado pelo(s) nosso(s) colaborador(es) em razão da execução da entrega?<b> " + questao05;
                            string msgEmailChecklist06 = "</b><br><b>Questão 06 -</b> Favor avalie a qualidade do serviço prestado: <b>" + questao06 + "</b><br><br>";
                            string msgChecklistTotal = msgEmailChecklist01 + msgEmailChecklist02 + msgEmailChecklist03 + msgEmailChecklist04 + msgEmailChecklist05 + msgEmailChecklist06;
                            conteudoEmail = msgEmail01 + msgEmail02 + msgEmail03 + msgEmail04 + msgEmail05 + msgEmail06 + msgChecklistTotal;

                            if (questao02 == "sim" || questao05 == "sim")
                            {
                                string msgEmailFoto = "<br><br><b>Foto 01 - </b>" + fotoAvaria01 + "<br><b>Foto 02 - </b>" + fotoAvaria02 + "<br><br>";
                                conteudoEmail = msgEmail01 + msgEmail02 + msgEmail03 + msgEmail04 + msgEmail05 + msgEmail06 + msgChecklistTotal + msgEmailFoto;

                            }
                        }
                        else
                        {
                            if (email != emailPagador2)
                            {
                                var auxiliarEmailCC = emailCC.ToString();
                                auxiliarEmailCC = emailPagador2;
                            }
                            conteudoEmail = msgEmail01 + msgEmail02 + msgEmail03 + msgEmail04 + msgEmail05 + msgEmail06;
                        }

                    }
                    else
                    {
                        string msgEmail01 = "Entrega não realizada! <br><br>Motivo: " + ocorrencia;
                        string msgEmail02 = "<br>Motorista: " + placa;
                        string msgEmail03 = "<br>Local: " + localDescricao + "<br><br>";
                        string msgEmail04 = pedidosNotas;
                        string msgEmail05 = "<br><br>";

                        conteudoEmail = msgEmail01 + msgEmail02 + msgEmail03 + msgEmail04 + msgEmail05;
                        assuntoEmail = "Retorno de Entrega";
                    }

                    #region Insert tabela EMAIL_BTI

                    string sqlInsertEmailBtiSemDados = File.ReadAllText(diretorioInsertEmailBti);
                    string sqlInsertEmailBtiCompleto = string.Format(sqlInsertEmailBtiSemDados,
                        tarefaIdArquivo,
                        emailRemetente,
                        smptEndereco,
                        senhaEmailRemetente,
                        emailDestinatario,
                        assuntoEmail,
                        emailBcc,
                        conteudoEmail,
                        statusEmail,
                        emailCC
                        );

                    if (File.Exists(diretorioValidaInsertEmailBti))
                    {
                        if (File.ReadAllText(diretorioValidaInsertEmailBti) == sqlInsertEmailBtiCompleto)
                        {

                        }
                        else
                        {
                            OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertEmailBtiCompleto, "indexes");
                            File.WriteAllText(diretorioValidaInsertEmailBti, sqlInsertEmailBtiCompleto);
                        }
                    }
                    else
                    {
                        OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertEmailBtiCompleto, "indexes");
                        File.WriteAllText(diretorioValidaInsertEmailBti, sqlInsertEmailBtiCompleto);
                    }
                    #endregion

                    EmailExportacao.enviar(conteudoEmail, assuntoEmail, smptEndereco, emailRemetente, nomeRemetente, senhaEmailRemetente, emailCC.ToString(), emailBcc, emailDestinatario, diretorioImagemAssinaturaRasador);

                    #endregion


                    try
                    {
                        File.Move(diretorioCompletoArquivoTarefa, diretorioCompletoArquivoTarefaAntigo);
                    }
                    catch
                    {
                        File.Delete(diretorioCompletoArquivoTarefaAntigo);
                        File.Move(diretorioCompletoArquivoTarefa, diretorioCompletoArquivoTarefaAntigo);
                        EscreverLogs.logInsucesso("Erro ao Mover - " + diretorioCompletoArquivoTarefa);
                    }

                }//for cada arquivo com tarefaId isolada.
            }
            #endregion

        }

        #region Métodos auxiliares

        public static void TratarPlacaPadraoMercosul(string placa)
        {
            //if (placa.)
            //{

            //}
        }

        public static void montarArquivoFinal(string dirLocal, string dirFTP, string placa, string nrNF_nrPedido, string nrCodigoBarras, string dataHora, string lido)
        {
            int indexParaCriarDiretorioLocal = dirLocal.LastIndexOf('/');
            string diretorioSemArquivoLocal = dirLocal.Substring(0, indexParaCriarDiretorioLocal + 1);

            int indexParaCriarDiretorioFTP = dirFTP.LastIndexOf('/');
            string diretorioSemArquivoFTP = dirLocal.Substring(0, indexParaCriarDiretorioFTP + 1);

            if (!Directory.Exists(diretorioSemArquivoLocal))
            {
                Directory.CreateDirectory(diretorioSemArquivoLocal);
            }
            if (!Directory.Exists(diretorioSemArquivoFTP))
            {
                Directory.CreateDirectory(diretorioSemArquivoFTP);
            }

            string linhaFormatada = placa;
            linhaFormatada = adicionaEspacoString(linhaFormatada, 12);
            linhaFormatada = linhaFormatada + nrNF_nrPedido;
            linhaFormatada = adicionaEspacoString(linhaFormatada, 32);
            linhaFormatada = linhaFormatada + nrCodigoBarras;
            linhaFormatada = adicionaEspacoString(linhaFormatada, 58);
            linhaFormatada = linhaFormatada + dataHora + lido;

            File.AppendAllText(dirLocal, linhaFormatada + Environment.NewLine);
            File.AppendAllText(dirFTP, linhaFormatada + Environment.NewLine);
        }

        public static string adicionaEspacoString(string str, int tamanhoString)
        {
            while (str.Length < tamanhoString)
            {
                str = str + " ";
            }
            return str;
        }

        //Método feito com o intuito de resolver o problema de variável vindo com valor DbNull.
        public static string verificaDbNull(dynamic valor)
        {
            if (DBNull.Value.Equals(valor) == true)
            {
                return string.Empty;
            }
            if (valor is string)
            {
                if (valor == string.Empty)
                {
                    return string.Empty;
                }
                else
                {
                    return (String)valor;
                }
            }
            if (valor is DateTime)
            {
                return ((DateTime)valor).ToString("dd/MM/yyyy");
            }
            if (valor is decimal numero)
            {
                int n = Convert.ToInt32(numero);
                return n.ToString();
            }
            return Convert.ToString(valor);
        }
        #endregion
    }
}
