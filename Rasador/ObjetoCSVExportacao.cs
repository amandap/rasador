﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    public class ObjetoCSVExportacao
    {
        public String TipoRegistro { get; set; }
        public String IdentificadorInternoIDdaTarefa { get; set; }
        public String TarefaIDparaIntegracao { get; set; }
        public String TarefaObservacao { get; set; }
        public String TarefaSituacao { get; set; }
        public String CodExecucaoAtividade { get; set; }
        public String StatusExecucaoAtividade { get; set; }
        public String DataHoraTarefa { get; set; }
        public String DataHoraInicioExecucaoAtividadeGeradaPeloSistemaDimTempo { get; set; }
        public String DataHoraFimExecucaoAtividadeGeradaPeloSistemaDimTempo { get; set; }
        public String LocalIdentificadoralternativo { get; set; }
        public String LocalDescricao { get; set; }
        public String PessoaIDparaIntegracao { get; set; }
        public String PessoaNome { get; set; }
        public String AtividadesIDparaintegracao { get; set; }
        public String AtividadesDescricao { get; set; }
        public String SecaoIDparaintegracao { get; set; }
        public String SecaoDescricao { get; set; }
        public String ItensIdentificador { get; set; }
        public String ItensDescricao { get; set; }
        public String Ocorrencia { get; set; }
        public String NOTAPEDIDO { get; set; }
        public String Lido { get; set; }
        public String Marca { get; set; }
        public String Codigo_de_Barras { get; set; }
        public String Total_Codigos { get; set; }
        public String Codigos_Lidos { get; set; }
        public String Codigos_Nao_Lidos { get; set; }
        public String X { get; set; }
        public String Foto_1 { get; set; }
        public String Foto_2 { get; set; }
        public String Foto_3 { get; set; }
        public String _1__O_local_da_entrega_esta_em_obras { get; set; }
        public String _2__Foi_constatada_alguma_avaria_nas_mercadorias { get; set; }
        public String _3__Verificou_se_os_vidros_e_portas_estao_em_perfeitas_condicoes { get; set; }
        public String _4__Os_produtos_foram_entregues_devidamente_e_no_local_solicitado { get; set; }
        public String _5__Constatou_alguma_alteracao_do_local_ou_fato_relevante_realizada_pelos_nossos_colaboradores_em { get; set; }
        public String _6__Favor_avalie_a_qualidade_do_servico_prestado { get; set; }
        public String X_Numerico { get; set; }
        public String Fotos_das_avarias { get; set; }
        public String Foto_da_Avaria_1 { get; set; }
        public String Foto_da_Avaria_2 { get; set; }
        public String Fotos_para_documentacao { get; set; }
        public String CPF { get; set; }
        public String Comprovante_de_Entrega { get; set; }
        public String Foto_cliente_ausente__estabelecimento_fechado { get; set; }
        public String PedidoNota { get; set; }
        public String Assinatura { get; set; }
    }
}
