﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class EmailExportacao
    {
        public static void enviar(string conteudoEmail, string assuntoEmail, string smtpEndereco, string emailRemetente, string nomeRemetente, string senhaRemetente, string emailCC, string emailBCC, string emailDestinatario, string diretorioImagem)
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(emailRemetente, nomeRemetente);

            SmtpClient smtp = new SmtpClient();
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mail.From.Address, senhaRemetente);
            smtp.Host = smtpEndereco;

            mail.To.Add(new MailAddress(emailDestinatario));
            if (emailCC != string.Empty)
            {
                mail.CC.Add(new MailAddress(emailCC));
            }
            mail.Bcc.Add(new MailAddress(emailBCC));
            mail.Bcc.Add(new MailAddress("franciner@btiestrategica.com.br"));
            mail.Bcc.Add(new MailAddress("guilhermer@btiestrategica.com.br"));

            //Formatted mail body
            LinkedResource res = new LinkedResource(diretorioImagem);
            res.ContentId = Guid.NewGuid().ToString();
            string htmlBody = conteudoEmail + @"<img src='cid:" + res.ContentId + @"'/>";
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(res);

            mail.Subject = assuntoEmail;
            mail.AlternateViews.Add(alternateView);
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }

    }
}
