﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class EmailImportacao
    {
        public static void enviar(string conteudoEmail)
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("entregas@rasador.com.br", "BTI");

            SmtpClient smtp = new SmtpClient();
            smtp.Port = 587;
            smtp.EnableSsl = false;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(mail.From.Address, "@gE$ra2015");
            smtp.Host = "mail.rasador.com.br";

            //recipient address
            mail.To.Add(new MailAddress("franciner@btiestrategica.com.br"));
            mail.To.Add(new MailAddress("guilhermer@btiestrategica.com.br"));
            mail.To.Add(new MailAddress("ti2@rasador.com.br"));

            //Formatted mail body
            mail.IsBodyHtml = true;
            mail.Subject = "Total de Tarefas Por Usuário";
            mail.Body = conteudoEmail;
            smtp.Send(mail);
        }
    }
}
