﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rasador
{
    class OperacoesPostgres
    {
        public static DataTable realizaSelectSql(string stringSQL)
        {
            //HARDCODE, NECESSÁRIO ALTERAR APÓS TÉRMINO DA PRIMEIRA FASE DA MIGRAÇÃO PARA C#
            string server = "107.23.73.6";
            string porta = "9999";
            string database = "prod_umov_dbview";
            string userId = "u13920";
            string password = "bti2013";
            string sslmode = "Require";
            string trustServerCertificate = "true";

            NpgsqlConnection conexaoPostgres = ConexaoPostgres.abrirConexao(server, porta, database, userId, password, sslmode, trustServerCertificate);
            if (conexaoPostgres != null)
            {
                NpgsqlDataAdapter comandoSQL = new NpgsqlDataAdapter(stringSQL, conexaoPostgres);
                DataTable resultadoSQL = new DataTable();
                comandoSQL.Fill(resultadoSQL); //preenche a tabela de acordo com o resultado gerado pelo SQL statement
                if (resultadoSQL != null)
                {
                    EscreverLogs.logSucesso(string.Format("O Sql Statement >>>{0}<<< foi executado com sucesso no banco de dados {1} - {2}.", stringSQL, server, database));
                }

                conexaoPostgres.Close();
                conexaoPostgres.Dispose();
                //retorna a tabela preenchida com o resultado do sql statement
                return resultadoSQL;
            }
            else
            {
                EscreverLogs.logInsucesso("Conexão retornou nula, impossibilitando a execução do Sql Statement.");
                return null;
            }
        }

        public static void realizaInsertDeleteUpdate(string stringSQL)
        {
            //HARDCODE, NECESSÁRIO ALTERAR APÓS TÉRMINO DA PRIMEIRA FASE DA MIGRAÇÃO PARA C#
            string server = "107.23.73.6";
            string porta = "9999";
            string database = "prod_umov_dbview";
            string userId = "u13920";
            string password = "bti2013";
            string sslmode = "Require";
            string trustServerCertificate = "true";

            NpgsqlConnection conexaoPostgres = ConexaoPostgres.abrirConexao(server, porta, database, userId, password, sslmode, trustServerCertificate);
            if (conexaoPostgres != null)
            {
                var comando = new NpgsqlCommand();
                comando.Connection = conexaoPostgres;
                comando.CommandText = stringSQL;
                try
                {
                    comando.Connection.Open();
                    int numeroLinhasAfetadas = comando.ExecuteNonQuery();
                    EscreverLogs.logSucesso(string.Format("O Sql Statement >|{0}|< foi executado com sucesso no banco de dados {1} - {2}. Número de linhas afetadas: {3}", stringSQL, server, database, numeroLinhasAfetadas));
                }
                catch (Exception e)
                {
                    EscreverLogs.logInsucesso(e.Message);
                }
                comando.Connection.Close();
                comando.Dispose();
                conexaoPostgres.Dispose();
            }
        }
    }
}
