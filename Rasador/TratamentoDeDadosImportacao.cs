﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using LayoutsUmovme;
using ClosedXML.Excel;

namespace Rasador
{
    class TratamentoDeDadosImportacao
    {
        //Nome das duas tabelas principais do código de importação. A tabela softran contém todos os dados que retornam do select feito no
        //banco de dados softran da tabela VW_ListaCodBarEntregas e que não estão nos dados retornados do select feito no banco de dados
        //umovmeapi na tabela dbo.indexes. A tabela indexes contém todos os dados que retornam do select feito no banco de dados umovmeapi
        //na tabela dbo.indexes e que não estão na tabela softran.
        public static string nomeTabelaComApenasTarefasSoftran = "TABELA TAREFAS SOFTRAN";
        public static string nomeTabelaComApenasTarefasIndexes = "TABELA TAREFAS INDEXES";


        //Listas que irão conter as linhas de dados dos arquivos que irão subir para Umov.me
        public static List<string> listaLinhasMontarLOC = new List<string>();
        public static List<string> listaLinhasMontarPSA = new List<string>();
        public static List<string> listaLinhasMontarITE = new List<string>();
        public static List<string> listaLinhasMontarIAG = new List<string>();
        public static List<string> listaLinhasMontarSGI = new List<string>();
        public static List<string> listaLinhasMontarGRI = new List<string>();
        public static List<string> listaLinhasMontarAGD = new List<string>();

        public static string comandoInsertUmovme = "I";
        public static string comandoDeleteUmovme = "D";
        public static string s = ";";


        /// <summary>
        /// Monta os arquivos no padrão Umov.me na pasta DiretorioCiclo onde o LimpaArquivos irá enviar ao FTP.
        /// </summary>
        public static void montarArquivosParaIntegracao()
        {
            string diretorioArquivoUmovmeAnterior = "C:/Integrator/ArquivosUmovme/DiretorioAnterior/";
            string diretorioHeaderHtmlEmail = "C:/Integrator/headerHtml.txt";
            string diretorioEndHtmlEmail = "C:/Integrator/endHtml.txt";
            string diretorioArquivosUmovme = "C:/Integrator/ArquivosUmovme/DiretorioCiclo/";
            string diretorioTxtComSqlStatementDbview = "C:/Integrator/selectDbview.txt";
            string diretorioTxtComSqlStatementDeleteCodebar = "C:/Integrator/deleteCodebar.txt";
            string diretorioTxtComSqlStatementDeleteIndexes = "C:/Integrator/deleteIndexes.txt";
            string diretorioTxtComSqlStatementAgentUmovme = "C:/Integrator/selectAgentUmovme.txt";
            string diretorioTxtComSqlStatementInsertCodebar = "C:/Integrator/insertCodebar.txt";
            string diretorioTxtComSqlStatementInsertIndexes = "C:/Integrator/insertIndexes.txt";
            string diretorioValidaInsertCodebar = "C:/Integrator/validaInsertCodebar.txt";
            string diretorioValidaInsertIndexes = "C:/Integrator/validaInsertIndexes.txt";
            string diretorioValidaDeleteCodebar = "C:/Integrator/validaDeleteCodebar.txt";
            string diretorioValidaDeleteIndexes = "C:/Integrator/validaDeleteIndexes.txt";
            string diretorioTxtComSqlStatementSoftran = "C:/Integrator/selectSoftran.txt";
            string diretorioTxtComSqlStatementUmovme = "C:/Integrator/selectIndexes.txt";
            string diretorioTxtComSqlStatementAgentDelete = "C:/Integrator/selectAgentUmovmeDelete.txt";
            string diretorioTxtComSqlStatementTarefasDelete = "C:/Integrator/selectTarefasDelete.txt";
            string diretorioTxtComSqlStatementDbviewAGDDelete = "C:/Integrator/selectDbviewAGDDelete.txt";
            string diretorioTxtComSqlStatementItinerantes = "C:/Integrator/selectItinerantes.txt";

            //Criação da tabela que é utilizada para criar as linhas de insert do arquivo AGD.
            var tabelaSemDadosGeral = new DataTable();
            tabelaSemDadosGeral.Columns.Add("TarefasId", typeof(string));
            tabelaSemDadosGeral.Columns.Add("qtdvolume", typeof(int));
            tabelaSemDadosGeral.Columns.Add("nrtransporte", typeof(string));
            tabelaSemDadosGeral.Columns.Add("nrcarga", typeof(string));
            tabelaSemDadosGeral.Columns.Add("nrpedido", typeof(string));
            tabelaSemDadosGeral.Columns.Add("fabricante", typeof(string));
            tabelaSemDadosGeral.Columns.Add("notafiscal", typeof(string));
            tabelaSemDadosGeral.Columns.Add("remetentes", typeof(string));
            tabelaSemDadosGeral.Columns.Add("destinatarios", typeof(string));
            tabelaSemDadosGeral.Columns.Add("industria", typeof(string));
            tabelaSemDadosGeral.Columns.Add("MSG2", typeof(string));
            tabelaSemDadosGeral.Columns.Add("AgentId", typeof(string));
            tabelaSemDadosGeral.Columns.Add("LocalId", typeof(string));
            tabelaSemDadosGeral.Columns.Add("observation", typeof(string));
            tabelaSemDadosGeral.Columns.Add("qntVol_NrNotaFiscal_NrPedido", typeof(string));
            tabelaSemDadosGeral.Columns.Add("DtSaiRom", typeof(string));
            tabelaSemDadosGeral.Columns.Add("Emails2", typeof(string));


            //Criação da tabela que vai conter todas as linhas que forem "GERAL"
            var tabelaComDadosGeral = new DataTable();
            tabelaComDadosGeral.Columns.Add("TarefasId", typeof(string));
            tabelaComDadosGeral.Columns.Add("qtdvolume", typeof(string));
            tabelaComDadosGeral.Columns.Add("nrpedido", typeof(string));
            tabelaComDadosGeral.Columns.Add("notafiscal", typeof(string));
            tabelaComDadosGeral.Columns.Add("industria", typeof(string));
            tabelaComDadosGeral.Columns.Add("MSG2", typeof(string));
            tabelaComDadosGeral.Columns.Add("observation", typeof(string));
            tabelaComDadosGeral.Columns.Add("AgentId", typeof(string));
            tabelaComDadosGeral.Columns.Add("LocalId", typeof(string));
            tabelaComDadosGeral.Columns.Add("qntVol_NrNotaFiscal_NrPedido", typeof(string));
            tabelaComDadosGeral.Columns.Add("DtSaiRom", typeof(string));
            tabelaComDadosGeral.Columns.Add("Emails2", typeof(string));

            DataTable tabelaComTodasTarefasIdSoftran = new DataTable();
            string nomeColunaTabelaComTodasTarefasIdSoftran = "TarefasIdSoftran";
            tabelaComTodasTarefasIdSoftran.Columns.Add(nomeColunaTabelaComTodasTarefasIdSoftran, typeof(string));

            DataTable tabelaParaEnvioEmail = new DataTable();
            string nomeColunaEmailAgente = "Agente";
            string nomeColunaEmailTarefaId = "TarefaId";
            tabelaParaEnvioEmail.Columns.Add(nomeColunaEmailAgente, typeof(string));
            tabelaParaEnvioEmail.Columns.Add(nomeColunaEmailTarefaId, typeof(string));

            DataTable tabelaParaEnvioEmailFiltrada = new DataTable();
            string nomeColunaEmailAgenteFiltrada = "Agente";
            string nomeColunaEmailTarefaIdFiltada = "TarefaId";
            tabelaParaEnvioEmailFiltrada.Columns.Add(nomeColunaEmailAgenteFiltrada, typeof(string));
            tabelaParaEnvioEmailFiltrada.Columns.Add(nomeColunaEmailTarefaIdFiltada, typeof(string));

            string comandoInsertUmovme = "I";
            string comandoDeleteUmovme = "D";
            //Variável utilizada para criar os arquivos .csv, usada como separador.
            string s = ";";
            DataTable dadosSelectSoftran = OperacoesSqlServer.realizaSelectSql(File.ReadAllText(diretorioTxtComSqlStatementSoftran), "softran");
            DataTable dadosItinerantes = OperacoesSqlServer.realizaSelectSql(File.ReadAllText(diretorioTxtComSqlStatementItinerantes), "softran");
            
            XLWorkbook xb = new XLWorkbook();
            xb.AddWorksheet(dadosItinerantes, "SelectItinerantes");
            xb.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaSqlItinerantes.xlsx");

            XLWorkbook xb1 = new XLWorkbook();
            xb1.AddWorksheet(dadosSelectSoftran, "SelectSoftran");
            xb1.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaSqlSoftran.xlsx");
            try
            {
                dadosSelectSoftran.Merge(dadosItinerantes);
            }
            catch(Exception e)
            {
                EscreverLogs.logInsucesso("Erro merge " + e.Message);
            }
            //verifica se a tabela contém dados
            if (dadosSelectSoftran.Rows.GetEnumerator().MoveNext())
            {
                #region Delete arquivo AGD para não entrar no Limpa Arquivos

                try
                {
                    File.Delete(diretorioArquivoUmovmeAnterior + "AGD_v2.csv");
                }
                catch (Exception e)
                {
                    EscreverLogs.logInsucesso(e.Message);
                }
                #endregion

                DataTable tabelaTarefasDbview = OperacoesPostgres.realizaSelectSql(File.ReadAllText(diretorioTxtComSqlStatementDbview));
                DataTable tabelaTarefasDbviewAgdDelete = OperacoesPostgres.realizaSelectSql(File.ReadAllText(diretorioTxtComSqlStatementDbviewAGDDelete));

                XLWorkbook xb4 = new XLWorkbook();
                xb4.AddWorksheet(tabelaTarefasDbview, "teste");
                xb4.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaTarefasDbView.xlsx");
                
                DataTable tabelaAgentsUmovme = OperacoesPostgres.realizaSelectSql(File.ReadAllText(diretorioTxtComSqlStatementAgentUmovme));

                for (int i = 0; i < dadosSelectSoftran.Rows.Count; i++)
                {
                    #region Variáveis retiradas da tabela e algumas condicionais (AQUI É CRIADO A VARIÁVEL tarefaId_Softran E industria)
                    /**
                     * Aqui estão as variáveis retiradas da tabela, abaixo também tem tratativas do email2, variável industria e a criação
                     * da variável TarefaId_Softran que é utilizada ao longo do código.
                     * 
                    */
                    string qntVolCte = verificaDbNull(dadosSelectSoftran.Rows[i]["QtVolNF"]);
                    string cte_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["CTe"]);
                    string nrCodigoBarras_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["NrCodigoBarras"]);
                    string nrSeriaNF_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["NrSeriaNF"]);
                    string pagador_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Pagador"]);
                    string emailAssistencia_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["EMailAssistencia"]);
                    string cnpj_rem_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["CNPJ_Rem"]);
                    string destinatario_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Destinatario"]).Replace(";", "");
                    string nrNF_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["NrNF"]);
                    string cnpj_dest_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["CNPJ_Dest"]);
                    string UF_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["UF"]);
                    string cidade_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Cidade"]);
                    string dsBairro_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["DsBairro"]);
                    string nrCep_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["NrCep"]);
                    string email_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["EMail"]);
                    string email2_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["EMail2"]);
                    if (email2_Softran == string.Empty)
                    {
                        email2_Softran = email_Softran;
                    }
                    else
                    {
                        email2_Softran = email2_Softran.Replace(";", "|");
                    }
                    string endereco_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Endereco"]);
                    string placa_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Placa"]);
                    if (!placa_Softran.Contains("-"))
                    {
                        placa_Softran = placa_Softran.Insert(2, "-");
                    }
                    string dtSaiRom_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Dt_Sai_Rom"]);
                    string nrPedidoNF_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["NrPedidoNF"]);
                    string pedidoParaLista = verificaDbNull(dadosSelectSoftran.Rows[i]["PedidoParaLista"]);
                    string dsProduto = verificaDbNull(dadosSelectSoftran.Rows[i]["DsProduto"]);
                    string Roman_Softran = verificaDbNull(dadosSelectSoftran.Rows[i]["Roman."]);
                    var placaPrimeiraParte_Softran = placa_Softran.Substring(0, 3);
                    var placaSegundaParte_Softran = placa_Softran.Substring(3, 5);
                    string industria = "";
                    string item_geralds = "";

                    string item_geralid = "";
                    //Bem intuitivo, se nrPedido for nulo OU pedidoParaLista for GERAL ou dsProduto for nulo, seta as variáveis para os
                    //valores abaixo.
                    if (nrPedidoNF_Softran == null || pedidoParaLista == "GERAL" || dsProduto == null)
                    {
                        industria = "1";
                        item_geralds = "GERAL";
                        item_geralid = "GERAL";
                    }
                    //caso não forem, seta para esses valores aqui.
                    else
                    {
                        industria = "0";
                        item_geralds = "S";
                        item_geralid = "S";
                    }
                    //TarefaId1 serve somente para montar a variável TarefaId, nomenclaturas devem ser arrumadas posteriormente, foram
                    //utilizadas as mesmas do pentaho.
                    string tarefaID1_Softran = (Roman_Softran + placaPrimeiraParte_Softran + placaSegundaParte_Softran + cnpj_dest_Softran + "0").Replace("-", " ").Replace(":", " ").Replace(".", " ");
                    string tarefaId_Softran = (tarefaID1_Softran + industria).Replace(" ", "");
                    string separador = "     -     ";
                    //string tarefaObs = nrNF_Softran + separador + nrPedidoNF_Softran + separador + email_Softran + separador + emailAssistencia_Softran + separador + cnpj_rem_Softran + separador + nrSeriaNF_Softran + separador + email2_Softran + separador;
                    //string tarefaObs = nrNF_Softran + separador + nrPedidoNF_Softran + separador + email_Softran + separador + emailAssistencia_Softran + separador + cnpj_rem_Softran + separador + nrSeriaNF_Softran + separador + email2_Softran;
                    string tarefaObs = cnpj_rem_Softran + separador + nrSeriaNF_Softran + separador + cte_Softran + separador + email_Softran + separador + " valor fixo " + separador + emailAssistencia_Softran + separador + pagador_Softran;
                    
                    #endregion
                    
                    try
                    {
                        #region Criação das linhas dos dados do arquivo LOC
                        string localDescricao_Softran = destinatario_Softran + "(" + cnpj_dest_Softran + ")";
                        string activeLOC = "1";
                        string localId = cnpj_dest_Softran;
                        string ufLoc = UF_Softran;
                        string cidadeLoc = cidade_Softran;
                        string linhaArquivoLOC = comandoInsertUmovme + s + localDescricao_Softran + s + activeLOC + s + s + localId + s + s + s + s + ufLoc + s + cidadeLoc + s + dsBairro_Softran + s + s + endereco_Softran + s + s + s + nrCep_Softran + s + s + s + s + s + s + s + email_Softran + s + s + s + s + s + s + cnpj_dest_Softran;
                        listaLinhasMontarLOC.Add(linhaArquivoLOC);
                        #endregion

                        #region Criação das linhas dos dados do arquivo PSA de Insert
                        int achouAgenteSoftran = 0;
                        for (int j = 0; j < tabelaAgentsUmovme.Rows.Count; j++)
                        {
                            string age_integrationId = verificaDbNull(tabelaAgentsUmovme.Rows[j]["age_integrationid"]);

                            if (placa_Softran == age_integrationId)
                            {
                                //está no softran e na umovme
                                achouAgenteSoftran = 1;
                                break;
                            }
                        }

                        //agente está no softran mas não está na umovme.
                        if (achouAgenteSoftran == 0)
                        {
                            string agentNome = placa_Softran;
                            string agentLogin = placa_Softran.Replace("-", string.Empty);
                            string agentPassword = placa_Softran.Substring(4, 4);
                            string tipoAgenteId = "1";
                            string activePSA = "1";
                            string agentId = placa_Softran;
                            string mobileUser = "1";
                            string emailAssistenciaPSA = emailAssistencia_Softran;
                            string changePassword = "0";
                            string memorizepasswordmobile = "0";
                            string linhaArquivoPSA = comandoInsertUmovme + s + s + agentNome + s + agentLogin + s + agentPassword + s + tipoAgenteId + s + activePSA + s + agentId + s + s + s + s + mobileUser + s + s + s + s + s + s + s + s + s + s + s + emailAssistenciaPSA + s + s + s + s + s + s + s + changePassword + s + memorizepasswordmobile + s + s + s;
                            listaLinhasMontarPSA.Add(linhaArquivoPSA);
                        }
                        #endregion

                        #region Criação das linhas dos dados do arquivo AGD dentro do for, INSERT INDEXES e CODEBAR, lista para EMAIL
                        string msg2 = "Pedido: <b>" + nrPedidoNF_Softran + "</b>, NF: <b>" + nrNF_Softran + "</b> <br>";
                        string qntVol_NrNotaFiscal_NrPedido = qntVolCte + "_" + nrNF_Softran + "_" + nrPedidoNF_Softran;
                        //if para separar os dados "GERAL" e os outros.


                        int achouTarefaId = 0;
                        for (int j = 0; j < tabelaTarefasDbview.Rows.Count; j++)
                        {
                            //dadoArquivoAGDComGeral.TarefaId.Substring(0, dadoArquivoAGDComGeral.TarefaId.Length - 1)
                            string tarefaIdDbview = verificaDbNull(tabelaTarefasDbview.Rows[j]["tsk_integrationid"]);

                            if (tarefaId_Softran == tarefaIdDbview.Substring(0, tarefaIdDbview.Length - 1) + "0" || tarefaId_Softran == tarefaIdDbview.Substring(0, tarefaIdDbview.Length - 1) + "1")
                            {
                                achouTarefaId = 1;
                                break;
                            }
                        }
                        if (achouTarefaId == 0)
                        {
                            if (industria == "0")
                            {
                                tabelaSemDadosGeral.Rows.Add(tarefaId_Softran, qntVolCte, "", "", nrPedidoNF_Softran, "", nrNF_Softran, "", "", industria, msg2, placa_Softran, cnpj_dest_Softran, tarefaObs, qntVol_NrNotaFiscal_NrPedido, dtSaiRom_Softran, email2_Softran);
                            }
                            if (industria == "1")
                            {
                                tabelaComDadosGeral.Rows.Add(tarefaId_Softran, qntVolCte, nrPedidoNF_Softran, nrNF_Softran, industria, msg2, tarefaObs, placa_Softran, cnpj_dest_Softran, qntVol_NrNotaFiscal_NrPedido, dtSaiRom_Softran, email2_Softran);
                            }

                            #region Criação das linhas dos dados do arquivo ITE, IAG, SGI, GRI e INSERT na tabela CODEBAR
                            string Datarom = dtSaiRom_Softran;
                            string activeITE = "1";
                            string categoriaId = "1";
                            string linhaArquivoITE = "";
                            string linhaArquivoIAG = "";

                            if (industria == "1")
                            {
                                string subGrupoGeral = "GERAL";
                                linhaArquivoIAG = comandoInsertUmovme + s + tarefaId_Softran + s + item_geralid;
                                linhaArquivoITE = comandoInsertUmovme + s + subGrupoGeral + s + item_geralds + s + item_geralid + s + activeITE + s + categoriaId + s + s + s + pedidoParaLista;
                                listaLinhasMontarIAG.Add(linhaArquivoIAG);
                                listaLinhasMontarITE.Add(linhaArquivoITE);
                            }
                            else
                            {
                                string sqlInsertCodebarSemDados = File.ReadAllText(diretorioTxtComSqlStatementInsertCodebar);
                                string sqlInsertCodebarCompleto = string.Format(sqlInsertCodebarSemDados, nrCodigoBarras_Softran, tarefaId_Softran, nrNF_Softran, nrPedidoNF_Softran, cnpj_rem_Softran);

                                #region Insert Codebar

                                if (File.Exists(diretorioValidaInsertCodebar))
                                {
                                    if (File.ReadAllText(diretorioValidaInsertCodebar) == sqlInsertCodebarCompleto)
                                    {

                                    }
                                    else
                                    {
                                        OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertCodebarCompleto, "indexes");
                                        File.WriteAllText(diretorioValidaInsertCodebar, sqlInsertCodebarCompleto);
                                    }
                                }
                                else
                                {
                                    OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertCodebarCompleto, "indexes");
                                    File.WriteAllText(diretorioValidaInsertCodebar, sqlInsertCodebarCompleto);
                                }
                                #endregion

                                string subGrupoId = nrNF_Softran + "-" + nrSeriaNF_Softran;
                                string activeSGI = "1";
                                string activeGRI = "1";
                                string grupoId = cte_Softran;
                                linhaArquivoIAG = comandoInsertUmovme + s + tarefaId_Softran + s + nrCodigoBarras_Softran;
                                linhaArquivoITE = comandoInsertUmovme + s + subGrupoId + s + dsProduto + s + nrCodigoBarras_Softran + s + activeITE + s + categoriaId + s + s + s + pedidoParaLista;
                                string linhaArquivoSGI = comandoInsertUmovme + s + subGrupoId + s + subGrupoId + s + grupoId + s + s + activeSGI;
                                string linhaArquivoGRI = comandoInsertUmovme + s + cte_Softran + s + grupoId + s + activeGRI;
                                listaLinhasMontarIAG.Add(linhaArquivoIAG);
                                listaLinhasMontarITE.Add(linhaArquivoITE);
                                listaLinhasMontarSGI.Add(linhaArquivoSGI);
                                listaLinhasMontarGRI.Add(linhaArquivoGRI);
                            }
                            #endregion

                            #region Insert Indexes

                            string dtSaiRomFormatado = DateTime.ParseExact(dtSaiRom_Softran, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("s");
                            string sqlInsertIndexesSemDados = File.ReadAllText(diretorioTxtComSqlStatementInsertIndexes);
                            string sqlInsertIndexesCompleto = string.Format(sqlInsertIndexesSemDados, Roman_Softran, placa_Softran, cnpj_dest_Softran, tarefaId_Softran, dtSaiRomFormatado, qntVol_NrNotaFiscal_NrPedido);


                            if (File.Exists(diretorioValidaInsertIndexes))
                            {
                                if (File.ReadAllText(diretorioValidaInsertIndexes) == sqlInsertIndexesCompleto)
                                {

                                }
                                else
                                {
                                    OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertIndexesCompleto, "indexes");
                                    File.WriteAllText(diretorioValidaInsertIndexes, sqlInsertIndexesCompleto);
                                }
                            }
                            else
                            {
                                OperacoesSqlServer.realizaInsertDeleteUpdate(sqlInsertIndexesCompleto, "indexes");
                                File.WriteAllText(diretorioValidaInsertIndexes, sqlInsertIndexesCompleto);
                            }
                            #endregion

                            tabelaParaEnvioEmail.Rows.Add(placa_Softran, tarefaId_Softran);
                        }
                        #endregion

                        tabelaComTodasTarefasIdSoftran.Rows.Add(tarefaId_Softran);
                    }
                    catch(Exception e)
                    {
                        EscreverLogs.logInsucesso("Erro da tarefaId " + tarefaId_Softran + " dentro do for. " + e.Message);
                    }
                }

                XLWorkbook xb5 = new XLWorkbook();
                xb5.AddWorksheet(tabelaComTodasTarefasIdSoftran, "test4");
                xb5.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaTarefasIdSoftran.xlsx");

                XLWorkbook xb6 = new XLWorkbook();
                xb6.AddWorksheet(tabelaParaEnvioEmail, "teste");
                xb6.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaEnvioEmail.xlsx");

                #region Select no Dbview e adiciona a lista de PSA os agentes a serem deletados

                DataTable tabelaAgentesDelete = OperacoesPostgres.realizaSelectSql(File.ReadAllText(diretorioTxtComSqlStatementAgentDelete));
                for (int i = 0; i < tabelaAgentesDelete.Rows.Count; i++)
                {
                    int achouAgente = 0;
                    string age_integrationId = verificaDbNull(tabelaAgentesDelete.Rows[i]["age_integrationid"]);

                    for (int j = 0; j < dadosSelectSoftran.Rows.Count; j++)
                    {
                        string placaSoftran = verificaDbNull(dadosSelectSoftran.Rows[j]["Placa"]);

                        if (age_integrationId == placaSoftran)
                        {
                            achouAgente = 1;
                            break;
                        }
                    }

                    if (achouAgente == 0)
                    {
                        string password = "123";
                        string tipoAgente = "1";
                        string deleteActive = "0";
                        string dMobileUser = "0";
                        string emailAssistencia = "";
                        string changePassword = "0";
                        string memorizepasswordmobile = "0";
                        string linhaArquivoPSA = comandoDeleteUmovme + s + "" + "" + s + s + s + password + s + tipoAgente + s + deleteActive + s + age_integrationId + s + s + s + s + s + dMobileUser + s + s + s + s + s + s + s + s + s + s + s + emailAssistencia + s + s + s + s + s + s + changePassword + s + memorizepasswordmobile + s + s + s;
                        listaLinhasMontarPSA.Add(linhaArquivoPSA);
                    }

                }

                #endregion

                #region Select no Dbview e adiciona a lista de AGD as tarefas a serem deletadas
                for (int i = 0; i < tabelaTarefasDbviewAgdDelete.Rows.Count; i++)
                {
                    string tsk_integrationid = verificaDbNull(tabelaTarefasDbviewAgdDelete.Rows[i]["tsk_integrationid"]);
                    int achouTarefaId = 0;
                    for (int j = 0; j < tabelaComTodasTarefasIdSoftran.Rows.Count; j++)
                    {
                        if (tsk_integrationid == verificaDbNull(tabelaComTodasTarefasIdSoftran.Rows[j][nomeColunaTabelaComTodasTarefasIdSoftran]))
                        {
                            achouTarefaId = 1;
                            break;
                        }
                    }

                    if (achouTarefaId == 0)
                    {
                        string linhaArquivoAGDdelete = comandoDeleteUmovme + s + s + s + s + s + s + s + s + s + s + tsk_integrationid + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s + s;
                        listaLinhasMontarAGD.Add(linhaArquivoAGDdelete);
                    }
                }
                #endregion

                #region Criação das linhas dos dados do arquivo AGD fora do for

                var dadosArquivoAGDSemGeral = from b in tabelaSemDadosGeral.AsEnumerable()
                                              group b by b.Field<string>("TarefasId") into g
                                              select new
                                              {
                                                  TarefaId = g.Key,
                                                  qntVol_NrNotaFiscal_NrPedido = g.Select(r => r.Field<string>("qntVol_NrNotaFiscal_NrPedido")).Distinct().OrderByDescending(d => d),
                                                  AgentId = g.Select(r => r.Field<string>("AgentId")).Distinct().OrderByDescending(d => d),
                                                  LocalId = g.Select(r => r.Field<string>("LocalId")).Distinct().OrderByDescending(d => d),
                                                  Observation = g.Select(r => r.Field<string>("observation")).Distinct().OrderByDescending(d => d),
                                                  Industria = g.Select(r => r.Field<string>("industria")).Distinct().OrderByDescending(d => d),
                                                  MSG3 = string.Join("", g.Select(r => r.Field<string>("MSG2")).Distinct().OrderByDescending(d => d)),
                                                  DataSaiRom = g.Select(r => r.Field<string>("DtSaiRom")).Distinct().OrderByDescending(d => d),
                                                  Email2 = g.Select(r => r.Field<string>("Emails2")).Distinct().OrderByDescending(d => d)
                                              };

                var dadosArquivoAGDComGeral = from b in tabelaComDadosGeral.AsEnumerable()
                                              group b by b.Field<string>("TarefasId") into g
                                              select new
                                              {
                                                  TarefaId = g.Key,
                                                  qntVol_NrNotaFiscal_NrPedido = g.Select(r => r.Field<string>("qntVol_NrNotaFiscal_NrPedido")).Distinct().OrderByDescending(d => d),
                                                  AgentId = g.Select(r => r.Field<string>("AgentId")).Distinct().OrderByDescending(d => d),
                                                  LocalId = g.Select(r => r.Field<string>("LocalId")).Distinct().OrderByDescending(d => d),
                                                  Industria = g.Select(r => r.Field<string>("industria")).Distinct().OrderByDescending(d => d),
                                                  Observation = g.Select(r => r.Field<string>("observation")).Distinct().OrderByDescending(d => d),
                                                  MSG3 = string.Join("", g.Select(r => r.Field<string>("MSG2")).Distinct()),
                                                  DataSaiRom = g.Select(r => r.Field<string>("DtSaiRom")).Distinct().OrderByDescending(d => d),
                                                  Email2 = g.Select(r => r.Field<string>("Emails2")).Distinct().OrderByDescending(d => d)
                                              };


                string hora_atual = "00:01";

                XLWorkbook xb2 = new XLWorkbook();
                xb2.AddWorksheet(tabelaComDadosGeral, "teste");
                xb2.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaComGERAL.xlsx");

                XLWorkbook xb3 = new XLWorkbook();
                xb3.AddWorksheet(tabelaSemDadosGeral, "teste");
                xb3.SaveAs("C:/Integrator/ResultadosSql/" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + "tabelaSemGERAL.xlsx");


                foreach (var dadoArquivoAGDSemGeral in dadosArquivoAGDSemGeral)
                {
                    string msg2SemGeral = "";
                    int quantidadeVolumeSemGeral = 0;

                    foreach (var dadoAuxiliarSemGeral in dadoArquivoAGDSemGeral.qntVol_NrNotaFiscal_NrPedido)
                    {
                        var dadosSplit = dadoAuxiliarSemGeral.Split('_');
                        int volume = 0;
                        try
                        {
                            volume = Convert.ToInt32(dadosSplit[0].ToString());
                        }
                        catch
                        {
                            EscreverLogs.logInsucesso("Erro ao converter volume da TarefaId " + dadoArquivoAGDSemGeral.TarefaId);
                        }
                        string numeroNotaFiscal = dadosSplit[1];
                        string numeroPedido = dadosSplit[2];
                        quantidadeVolumeSemGeral = volume + quantidadeVolumeSemGeral;
                        msg2SemGeral = "Pedido: <b>" + numeroPedido + "</b>, NF: <b>" + numeroNotaFiscal + "</b> <br>" + msg2SemGeral;
                    }

                    //string observationSemGeral = string.Join("---", dadoArquivoAGDSemGeral.Observation);
                    string activeAGD = "1";
                    string activitiesOrigin = "3";
                    string priority = "1";
                    string recreateTaskOnPda = "0";
                    string nrtransporte = "0";
                    string nrcarga = "0";
                    string nrpedido = "0";
                    string fabricante = "0";
                    string notasfiscais = "0";
                    string remetentes = "0";
                    string destinatarios = "0";

                    int achouGeralNaTarefaId = 0;
                    foreach (var dadoArquivoAGDComGeral in dadosArquivoAGDComGeral)
                    {
                        //quanto tarefaID Com geral e tarefaID Sem geral forem iguais.
                        if (dadoArquivoAGDComGeral.TarefaId.Substring(0, dadoArquivoAGDComGeral.TarefaId.Length - 1) + "0" == dadoArquivoAGDSemGeral.TarefaId)
                        {
                            string msg2ComGeral = "";
                            int quantidadeVolumeComGeral = 0;

                            foreach (var dadoAuxiliarComGeral in dadoArquivoAGDComGeral.qntVol_NrNotaFiscal_NrPedido)
                            {
                                var dadosSplit = dadoAuxiliarComGeral.Split('_');
                                int volume = 0;
                                try
                                {
                                    volume = Convert.ToInt32(dadosSplit[0].ToString());
                                }
                                catch
                                {
                                    EscreverLogs.logInsucesso("Erro ao converter volume da TarefaId " + dadoArquivoAGDComGeral.TarefaId);
                                }
                                string numeroNotaFiscal = dadosSplit[1];
                                string numeroPedido = dadosSplit[2];

                                quantidadeVolumeComGeral = volume + quantidadeVolumeComGeral;
                                msg2ComGeral = "Pedido: <b>" + numeroPedido + "</b>, NF: <b>" + numeroNotaFiscal + "</b> <br>" + msg2ComGeral;

                            }

                            //string observationComGeral = string.Join("---", dadoArquivoAGDComGeral.Observation);
                            //string observationAtualizado = observationSemGeral + observationComGeral;
                            int quantidadeVolumeAtualizado = quantidadeVolumeSemGeral + quantidadeVolumeComGeral;
                            string msg3Atualizado = msg2SemGeral + msg2ComGeral;
                            //observationAtualizado
                            string linhaArquivoAGD = comandoInsertUmovme + s + s + dadoArquivoAGDSemGeral.AgentId.First() + s + s + s + dadoArquivoAGDSemGeral.LocalId.First() + s + dadoArquivoAGDSemGeral.DataSaiRom.First() + s + hora_atual + s + activeAGD + s + activitiesOrigin + s + dadoArquivoAGDSemGeral.TarefaId + s + s + s + s + recreateTaskOnPda + s + s + s + s + s + s + s + priority + s + dadoArquivoAGDSemGeral.Observation.First() + s + quantidadeVolumeAtualizado + s + nrtransporte + s + nrcarga + s + nrpedido + s + fabricante + s + notasfiscais + s + remetentes + s + destinatarios + s + dadoArquivoAGDSemGeral.Industria.First() + s + verificaTamanhoString(msg3Atualizado) + s + dadoArquivoAGDSemGeral.Email2.First();
                            listaLinhasMontarAGD.Add(linhaArquivoAGD);
                            achouGeralNaTarefaId = 1;
                            break;
                        }
                    }
                    //QUANDO TAREFAID NÃO TEM GERAL.
                    if (achouGeralNaTarefaId == 0)
                    {
                        //observationSemGeral
                        string linhaArquivoAGD = comandoInsertUmovme + s + s + dadoArquivoAGDSemGeral.AgentId.First() + s + s + s + dadoArquivoAGDSemGeral.LocalId.First() + s + dadoArquivoAGDSemGeral.DataSaiRom.First() + s + hora_atual + s + activeAGD + s + activitiesOrigin + s + dadoArquivoAGDSemGeral.TarefaId + s + s + s + s + recreateTaskOnPda + s + s + s + s + s + s + s + priority + s + dadoArquivoAGDSemGeral.Observation.First() + s + quantidadeVolumeSemGeral + s + nrtransporte + s + nrcarga + s + nrpedido + s + fabricante + s + notasfiscais + s + remetentes + s + destinatarios + s + dadoArquivoAGDSemGeral.Industria.First() + s + verificaTamanhoString(msg2SemGeral) + dadoArquivoAGDSemGeral.Email2.First();
                        listaLinhasMontarAGD.Add(linhaArquivoAGD);
                        //TarefaId não tem Geral
                    }
                }

                //FAZER ESSA PARTE CASO EXISTA TAREFAS ID QUE TEM SOMENTE GERAL E MAIS NENHUM DADO.
                foreach (var dadoArquivoAGDComGeral in dadosArquivoAGDComGeral)
                {
                    int achouTarefaId = 0;
                    foreach (var dadoArquivoSemGeral in dadosArquivoAGDSemGeral)
                    {
                        if (dadoArquivoAGDComGeral.TarefaId.Substring(0, dadoArquivoAGDComGeral.TarefaId.Length - 1) + "0" == dadoArquivoSemGeral.TarefaId)
                        {
                            achouTarefaId = 1;
                            break;
                        }
                    }
                    if (achouTarefaId == 0)
                    {
                        int quantidadeVolumeComGeralApenas = 0;
                        string msg2ComGeralApenas = "";

                        foreach (var dadoAuxiliarComGeral in dadoArquivoAGDComGeral.qntVol_NrNotaFiscal_NrPedido)
                        {
                            var dadosSplit = dadoAuxiliarComGeral.Split('_');
                            int volume = 0;
                            try
                            {
                                volume = Convert.ToInt32(dadosSplit[0].ToString());
                            }
                            catch(Exception e)
                            {
                                EscreverLogs.logInsucesso("Erro ao converter volume da TarefaId " + dadoArquivoAGDComGeral.TarefaId);
                            }
                            string numeroNotaFiscal = dadosSplit[1];
                            string numeroPedido = dadosSplit[2];
                            quantidadeVolumeComGeralApenas = volume + quantidadeVolumeComGeralApenas;
                            msg2ComGeralApenas = "Pedido: <b>" + numeroPedido + "</b>, NF: <b>" + numeroNotaFiscal + "</b> <br>" + msg2ComGeralApenas;
                        }
                        //string observationComGeral = string.Join("---", dadoArquivoAGDComGeral.Observation);
                        string activeAGD = "1";
                        string activitiesOrigin = "3";
                        string priority = "1";
                        string recreateTaskOnPda = "0";
                        string nrtransporte = "0";
                        string nrcarga = "0";
                        string nrpedido = "0";
                        string fabricante = "0";
                        string notasfiscais = "0";
                        string remetentes = "0";
                        string destinatarios = "0";

                        //File.AppendAllText("C:/Integrator/arquivoApenasComGeral_1.txt", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " - " + dadoArquivoAGDComGeral.TarefaId + Environment.NewLine);

                        string linhaArquivoAGD = comandoInsertUmovme + s + s + dadoArquivoAGDComGeral.AgentId.First() + s + s + s + dadoArquivoAGDComGeral.LocalId.First() + s + dadoArquivoAGDComGeral.DataSaiRom.First() + s + hora_atual + s + activeAGD + s + activitiesOrigin + s + dadoArquivoAGDComGeral.TarefaId + s + s + s + s + recreateTaskOnPda + s + s + s + s + s + s + s + priority + s + dadoArquivoAGDComGeral.Observation.First() + s + quantidadeVolumeComGeralApenas + s + nrtransporte + s + nrcarga + s + nrpedido + s + fabricante + s + notasfiscais + s + remetentes + s + destinatarios + s + dadoArquivoAGDComGeral.Industria.First() + s + verificaTamanhoString(msg2ComGeralApenas) + s + dadoArquivoAGDComGeral.Email2.First();
                        listaLinhasMontarAGD.Add(linhaArquivoAGD);
                    }
                }
                #endregion
                
                #region Envio de email com os dados dos agentes e a quantidade de tarefas
                for (int i = 0; i < tabelaParaEnvioEmail.Rows.Count; i++)
                {
                    string agenteIdInicial = verificaDbNull(tabelaParaEnvioEmail.Rows[i]["Agente"]);
                    string tarefaIdInicial = verificaDbNull(tabelaParaEnvioEmail.Rows[i]["TarefaId"]);
                    int achouTarefaId = 0;
                    if (tarefaIdInicial.Last() == '1')
                    {
                        for (int j = 0; j < tabelaParaEnvioEmail.Rows.Count; j++)
                        {
                            string tarefaIdComparacao = verificaDbNull(tabelaParaEnvioEmail.Rows[j]["TarefaId"]);
                            if (tarefaIdInicial.Substring(0, tarefaIdInicial.Length - 1) + "0" == tarefaIdComparacao)
                            {
                                achouTarefaId = 1;
                                break;
                            }
                        }
                        if (achouTarefaId == 0)
                        {
                            tabelaParaEnvioEmailFiltrada.Rows.Add(agenteIdInicial, tarefaIdInicial);
                        }
                    }
                    else
                    {
                        tabelaParaEnvioEmailFiltrada.Rows.Add(agenteIdInicial, tarefaIdInicial);
                    }

                }

                var dadosAgentesParaEmail = from b in tabelaParaEnvioEmailFiltrada.AsEnumerable()
                                            group b by b.Field<string>(nomeColunaEmailAgente) into g
                                            select new
                                            {
                                                AgentId = g.Key,
                                                Tarefas = g.Select(r => r.Field<string>(nomeColunaEmailTarefaId))
                                            };
                string headerHtml = File.ReadAllText(diretorioHeaderHtmlEmail);
                string endHtml = File.ReadAllText(diretorioEndHtmlEmail);
                string bodyHtml = "";
                var listaBody = new List<string>();
                foreach (var dadoAgente in dadosAgentesParaEmail)
                {
                    int quantidadeTarefas = dadoAgente.Tarefas.Distinct().Count();
                    string primeiraParteBody = "<tr><td align='center'>";
                    string segundaParteBody = "</td><td align='center'>";
                    string terceiraParteBoddy = "</td></tr><tr>";
                    bodyHtml = primeiraParteBody + dadoAgente.AgentId + segundaParteBody + quantidadeTarefas + terceiraParteBoddy;
                    listaBody.Add(bodyHtml);
                }
                if (listaBody.Any())
                {
                    bodyHtml = string.Join("", listaBody);
                }
                else
                {
                    headerHtml = "";
                    bodyHtml = "Não existem novas tarefas.";
                    endHtml = "";
                }

                string conteudoEmail = headerHtml + bodyHtml + endHtml;
                EmailImportacao.enviar(conteudoEmail.Replace("'", "\""));






                #endregion

            }

            #region Criação do arquivo ITE
            string nomeArquivoITE = "ITE_v2.csv";
            string diretorioFinalArquivoITE = diretorioArquivosUmovme + nomeArquivoITE;
            string customFieldsITE = ";CF_marca";
            ITE.CriaArquivoITE("C", diretorioFinalArquivoITE, customFieldsITE, listaLinhasMontarITE.Distinct().ToArray());
            #endregion

            #region Criação do arquivo LOC
            string nomeArquivoLOC = "LOC_v2.csv";
            string diretorioFinalArquivoLOC = diretorioArquivosUmovme + nomeArquivoLOC;
            string customFieldsLOC = ";CF_cnpj";
            LOC.CriaArquivoLOC("C", diretorioFinalArquivoLOC, customFieldsLOC, listaLinhasMontarLOC.Distinct().ToArray());
            #endregion

            #region Criação do arquivo PSA
            string nomeArquivoPSA = "PSA_v2.csv";
            string diretorioFinalArquivoPSA = diretorioArquivosUmovme + nomeArquivoPSA;
            string customFieldsPSA = "";
            PSA.CriaArquivoPSA("C", diretorioFinalArquivoPSA, customFieldsPSA, listaLinhasMontarPSA.Distinct().ToArray());
            #endregion

            #region Criação do arquivo AGD
            string nomeArquivoAGD = "AGD_v2.csv";
            string diretorioFinalArquivoAGD = diretorioArquivosUmovme + nomeArquivoAGD;
            string customFieldsAGD = ";CF_qtdvolume;CF_nrtransporte;CF_nrcarga;CF_nrpedido;CF_fabricante;CF_notasfiscais;CF_remetentes;CF_destinatarios;CF_industria;CF_PedidoNota;CF_EMAILS_2";
            AGD.CriaArquivoAGD("C", diretorioFinalArquivoAGD, customFieldsAGD, listaLinhasMontarAGD.Distinct().ToArray());
            #endregion

            #region Criação do arquivo IAG
            string nomeArquivoIAG = "IAG_v2.csv";
            string diretorioFinalArquivoIAG = diretorioArquivosUmovme + nomeArquivoIAG;
            string customFieldsIAG = "";
            IAG.CriaArquivoIAG("C", diretorioFinalArquivoIAG, customFieldsIAG, listaLinhasMontarIAG.Distinct().ToArray());
            #endregion

            #region Criação do arquivo SGI
            string nomeArquivoSGI = "SGI_v2.csv";
            string diretorioFinalArquivoSGI = diretorioArquivosUmovme + nomeArquivoSGI;
            SGI.CriaArquivoSGI("C", diretorioFinalArquivoSGI, listaLinhasMontarSGI.Distinct().ToArray());
            #endregion

            #region Criação do arquivo GRI
            string nomeArquivoGRI = "GRI_v2.csv";
            string diretorioFinalArquivoGRI = diretorioArquivosUmovme + nomeArquivoGRI;
            GRI.CriaArquivoGRI("C", diretorioFinalArquivoGRI, listaLinhasMontarGRI.Distinct().ToArray());
            #endregion

        }

        //Método feito com o intuito de resolver o problema de variável vindo com valor DbNull.
        public static string verificaDbNull(dynamic valor)
        {
            if (DBNull.Value.Equals(valor) == true)
            {
                return string.Empty;
            }
            if (valor is string)
            {
                if (valor == string.Empty)
                {
                    return string.Empty;
                }
                else
                {
                    return (String)valor;
                }
            }
            if (valor is DateTime)
            {
                return ((DateTime)valor).ToString("dd/MM/yyyy");
            }
            if (valor is decimal numero)
            {
                int n = Convert.ToInt32(numero);
                return n.ToString();
            }
            return Convert.ToString(valor);
        }

        public static string verificaTamanhoString(string str)
        {
            if (str.Length > 1998)
            {
                str = str.Substring(0, 1998);
                return str;
            }
            else
            {
                return str;
            }
        }
    }
}